package com.example.easikoufu.utils

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import com.example.easikoufu.ui.MainActivity

class BootUpReceiver : BroadcastReceiver() {
    override fun onReceive(context: Context, intent: Intent) {
        val i = MainActivity.newIntent(context)
        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
        context.startActivity(i)
    }
}