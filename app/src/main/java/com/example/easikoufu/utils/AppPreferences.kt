package com.example.easikoufu.utils

import android.content.Context
import android.content.SharedPreferences
import androidx.appcompat.app.AppCompatActivity
import com.google.gson.Gson
import com.google.gson.GsonBuilder

class AppPreferences(context: Context) : Constants() {

    private val mPrefs: SharedPreferences
    private val gson: Gson

    init {
        mPrefs = context.getSharedPreferences(PREFNAME, AppCompatActivity.MODE_PRIVATE)
        gson = GsonBuilder().create()
    }
}