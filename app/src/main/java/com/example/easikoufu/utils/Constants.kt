package com.example.easikoufu.utils

open class Constants {
    companion object {
        const val PREFNAME = "app_preferences"
        const val urlRegex = "https?:\\/\\/(www\\.)?[-a-zA-Z0-9@:%._\\+~#=]{1,256}\\.[a-zA-Z0-9()]{1,6}\\b([-a-zA-Z0-9()@:%_\\+.~#?&//=]*)"
    }
}