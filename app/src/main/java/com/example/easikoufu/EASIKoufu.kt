package com.example.easikoufu

import android.app.Activity
import android.app.Application
import android.os.Bundle
import com.example.easikoufu.utils.AppPreferences
import com.facebook.drawee.backends.pipeline.Fresco
import com.facebook.stetho.Stetho

class EASIKoufu : Application() {

    var mActivity: Activity? = null

    companion object {
        var appPreferences: AppPreferences? = null
    }

    override fun onCreate() {
        super.onCreate()
        appPreferences = AppPreferences(this)
        Fresco.initialize(this)
        Stetho.initializeWithDefaults(this)

        setupLifeCycleCallback()
    }

    private fun setupLifeCycleCallback() {
        registerActivityLifecycleCallbacks(object : ActivityLifecycleCallbacks {
            override fun onActivityCreated(activity: Activity, bundle: Bundle?) {

            }
            override fun onActivityStarted(activity: Activity) {
                mActivity = activity
            }

            override fun onActivityResumed(activity: Activity) {
                mActivity = activity
            }

            override fun onActivityPaused(activity: Activity) {
                mActivity = null
            }

            override fun onActivityStopped(activity: Activity) {

            }
            override fun onActivitySaveInstanceState(activity: Activity, bundle: Bundle) {

            }
            override fun onActivityDestroyed(activity: Activity) {

            }
        })
    }
}