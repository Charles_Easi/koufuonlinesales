package com.example.easikoufu.ui

import android.content.Context
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.view.KeyEvent
import android.view.View
import android.webkit.CookieManager
import android.webkit.WebSettings
import androidx.appcompat.app.AppCompatActivity
import com.example.easikoufu.databinding.ActivityMainBinding
import com.example.easikoufu.utils.Constants.Companion.urlRegex
import com.example.easikoufu.utils.CustomWebViewClient

class MainActivity : AppCompatActivity() {

    private var binding: ActivityMainBinding? = null

    companion object {
        fun newIntent(context: Context): Intent {
            return Intent(context, MainActivity::class.java)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding!!.root)

        setupWebView()
    }

    private fun setupWebView() {
        binding!!.webView.apply {
            this.webViewClient = CustomWebViewClient(application, object : CustomWebViewClient.WebViewClientListener {
                override fun onReceivedError(failingUrl: String?) {

                }
            })
            this.settings.apply {
                this.loadWithOverviewMode = true
                this.useWideViewPort = false
                this.loadsImagesAutomatically = true
                this.builtInZoomControls = true
                this.displayZoomControls = false
                this.layoutAlgorithm = WebSettings.LayoutAlgorithm.TEXT_AUTOSIZING
                this.javaScriptEnabled = true
                clearCache(true)
                clearFormData()
                clearHistory()
                clearSslPreferences()
            }
            this.setOnKeyListener { _, keyCode, event ->
                if (event.action == KeyEvent.ACTION_DOWN) {
                    when (keyCode) {
                        KeyEvent.KEYCODE_BACK -> if (binding!!.webView.canGoBack()) {
                            binding!!.webView.goBack()
                            return@setOnKeyListener true
                        }
                    }
                }
                return@setOnKeyListener false
            }
            loadWebViewUrl()
        }
    }

    private fun loadWebViewUrl() {
        val url = "http://118.189.155.210:5002/EASIKoufuOnline/public/login"
        val regex = urlRegex.toRegex()
        if (url.isNotBlank() && regex.containsMatchIn(url)) {
            binding!!.webView.loadUrl(url)
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        binding = null
//        binding!!.webView.clearCache(true)
//        binding!!.webView.clearFormData()
//        binding!!.webView.clearHistory();
//        binding!!.webView.clearSslPreferences();
    }


    override fun onBackPressed() {
        if (binding!!.webView.canGoBack()) {
            binding!!.webView.goBack()
        }
    }

    //region Lifecycle
    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        binding!!.webView.saveState(outState)
    }

    override fun onRestoreInstanceState(savedInstanceState: Bundle) {
        super.onRestoreInstanceState(savedInstanceState)
        binding!!.webView.restoreState(savedInstanceState)
    }
}